import java.util.Scanner;	
public class GameLauncher{
	
	public static void main(String[] args){ 
		Scanner reader = new Scanner(System.in); 
		System.out.println("Enter 1 to play Hangman, or Enter 2 to play Wordle.");
		int input = reader.nextInt();
		//ask the user to enter 1 or 2 and records what they type as input
		while(input != 1 && input != 2){
			System.out.println("Invalid value, type 1 or 2");
			input = reader.nextInt();
		}
		//while loop keeps telling the user that they have inputted an invalid value and repeats until they input a value that is either 1 or 2
		if(input == 1){
			System.out.println("Welcome to Hangman!" + "\n" + "Write a 4 letter word:");
			String chosenWord = reader.next();
			hangman.runGame(chosenWord);
		}
		//if input is 1, start the hangman game. Ask the user to input a 4 letter word, record it as chosenWord, and run the runGame method from hangman.java with the chosenWord. 
		else{
			String target = wordle.generateWord();
			wordle.runGame(target);
		}
		//if the input is 2, start wordle. Uses method generateWord from wordle.java to generate one of the target words, then uses method runGame from wordle.java to start the game with the target word. 
	}
}
