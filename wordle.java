import java.util.Scanner;
import java.util.Random;

public class wordle{
	
	public static String generateWord(){
		Random rando = new Random();
		String[] list = {"THANK", "WHALE", "CHANT", "DEMON", "STOCK", "SLATE", "SPENT", "PARTY", "INPUT", "TEACH", "THIRD", "SMART", "ENJOY", "AMONG", "SHOCK", "BROKE", "MAYBE", "CLEAR", "BIRTH", "ALIKE", "INDEX", "SMITH", "EXIST", "QUIET", "PRIME", "RATIO"};
		int x = rando.nextInt(list.length);
		return list[x]; 
	}
	
	public static boolean letterInWord(String word, char letter){
		boolean condition1 = false;
		for(int i=0; i<word.length(); i++){
			if(word.charAt(i)==(letter)){
				condition1 = true;
			}
		}
		if(condition1 == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static boolean letterInSlot(String word, char letter, int position){
		if(word.charAt(position)==letter){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static String[] guessWord(String guess, String answer){
		String[] colors = new String[5];
		for(int i = 0; i<answer.length(); i++){
			if(letterInWord(answer, guess.charAt(i))==true){
				if(letterInSlot(answer, guess.charAt(i), i)==true){
					colors[i] = "green";
				}
				else{
					colors[i] = "yellow";
				}
			}
			else{
				colors[i] = "white";
			}
		}
		return colors;
	}
	
	public static void presentResults(String word, String[] colours){
		for(int i = 0; i<colours.length; i++){
			if(colours[i].equals("green")){
				System.out.println("\u001B[32m" + word.charAt(i) + "\u001B[0m");
			}
			if(colours[i].equals("yellow")){
				System.out.println("\u001B[33m" + word.charAt(i) + "\u001B[0m");
			}
			if(colours[i].equals("white")){
				System.out.println(word.charAt(i));
			}
		}
	}
	
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Write a 5 letter word");
		String input = reader.nextLine();
		while(input.length() != 5){
			System.out.println("That's not 5 letters u bozo");
			input = reader.nextLine();
		}
		return input.toUpperCase();
	}
	
	public static void runGame(String word){
		int attempts = 0;
		boolean runCondition = false;
		System.out.println("Welcome to Wordle!");
		while(runCondition == false && attempts<6){
			String input = readGuess();
			presentResults(input, guessWord(input, word));
			if(input.equals(word)){
				System.out.println("Congrats! You win.");
				runCondition = true;
			}
			attempts++;
			System.out.println("Attempts: " + attempts);
			System.out.println("---------------------");
		}
		if(attempts == 6){
			System.out.println("You fail. The word was: " + word + ". Try Again!");
		}
	}
}