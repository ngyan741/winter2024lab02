import java.util.Scanner; //imports Scanner to detect user input

public class hangman{

	//function that checks if the given character is in the word
	public static int isLetterInWord(String word, char c){ 
		
		for(int i = 0; i<word.length(); i++){
			if(toUpperCase(word.charAt(i))==(toUpperCase(c))){
				return i; 
			}
		}
		return -1;
	}
	//function that turns all characters given to uppercaseS
	public static char toUpperCase(char c){ 
		return Character.toUpperCase(c);
	}

	
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){ 
		if(letter0){
			System.out.println(toUpperCase(word.charAt(0)));
		}
		else{
			System.out.println("_");
		}
		if(letter1){
			System.out.println(toUpperCase(word.charAt(1)));
		}
		else{
			System.out.println("_");
		}
		if(letter2){
			System.out.println(toUpperCase(word.charAt(2)));
		}
		else{
			System.out.println("_");
		}
		if(letter3){
			System.out.println(toUpperCase(word.charAt(3)));
		}
		else{
			System.out.println("_");
		}
	}
	//main game function that establishes the variables and runs a continuous loop that keeps the game running until the user has guessed all the characters or have guessed wrong 6 times.
	public static void runGame(String word){  
		boolean[] letters = {false, false, false, false};
		int misses = 0;
		
		while((letters[0] == false || letters[1] == false || letters[2] == false || letters[3] == false)&& misses < 6){
			Scanner reader = new Scanner(System.in);
			System.out.println("Type a character:");
			char input2 = reader.next().charAt(0);
			int output = isLetterInWord(word, input2);
			if(output == 0){
				letters[0] = true;
				System.out.println("Correct!");
			}
			else if(output == 1){ 
				letters[1] = true;
				System.out.println("Correct!");
			}
			else if(output == 2){
				letters[2] = true;
				System.out.println("Correct!");
			}
			else if(output == 3){
				letters[3] = true;
				System.out.println("Correct!");
			}
			else{
				misses++; 
				System.out.println("Misses: " + misses);
			}
			printWord(word, letters[0], letters[1], letters[2], letters[3]);
		}	
	}
}

